# Déploiement rapide de CAS
function cas-deploy {
    rm -rf /etc/chl/cas5/ /opt/cas-server-5/target/ ${CATALINA_HOME}/webapps/cas* ${CATALINA_HOME}/{logs,work} /var/{cache,log}/${TOMCAT8_USER}/;
    install -m 750 -o ${TOMCAT8_USER} -g adm -d /var/{cache,log}/${TOMCAT8_USER}/;
    ln -s /var/log/${TOMCAT8_USER}/ ${CATALINA_HOME}/logs;
    ln -s /var/cache/${TOMCAT8_USER}/ ${CATALINA_HOME}/work;
    systemctl restart tomcat8.service &&
    pushd /opt/cas-server-5/ &&
    mvn clean package -e &&
    cp /opt/cas-server-5/target/cas.war ${CATALINA_HOME}/webapps/ &&
    install -m 755 -o root -g root -d /etc/chl/ &&
    install -m 750 -o ${TOMCAT8_USER} -g ${TOMCAT8_GROUP} -d /etc/chl/cas5/ &&
    install -m 640 -o ${TOMCAT8_USER} -g ${TOMCAT8_GROUP} {.,}/etc/chl/cas5/log4j2.xml &&
    cp -R {.,}/etc/chl/cas5/config/ &&
    cp -R {.,}/etc/chl/cas5/services/ &&
    cp -R {.,}/etc/chl/cas5/kerberos/ &&
    chown -R ${TOMCAT8_USER}:${TOMCAT8_GROUP} /etc/chl/cas5/{config,services}/ &&
    chown -R root:${TOMCAT8_GROUP} /etc/chl/cas5/kerberos/ &&
    chmod -R 640 /etc/chl/cas5/{config,kerberos,services}/ &&
    chmod 750 /etc/chl/cas5/{config,kerberos,services}/ &&
    systemctl restart tomcat8.service &&
    popd
}
