export HOSTNAME=appli1
export DOMAIN=miletrie.lan

cat <<-EOF >/etc/network/interfaces
# Loopback NIC
auto lo
iface lo inet loopback

# Ethernet NIC
auto eth0
allow-hotplug eth0
iface eth0 inet static
    address 172.16.57.148
    netmask 255.255.0.0
    gateway 172.16.10.1
    dns-search ${DOMAIN}
    dns-nameservers 172.16.57.146 172.16.10.32 172.16.10.33

EOF
cat <<-EOF >/etc/resolv.conf
search miletrie.lan
domain miletrie.lan
nameserver 172.16.57.146

EOF
ifdown eth0 && ifup eth0

cat <<-EOF >/etc/hostname
${HOSTNAME}
EOF
cat <<-EOF >/etc/hosts
# IPv4
127.0.0.1 localhost
127.0.1.1 ${HOSTNAME}.${DOMAIN} ${HOSTNAME}

# IPv6
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

EOF
hostname ${HOSTNAME}
hostname -b ${HOSTNAME}
domainname ${HOSTNAME}.${DOMAIN}
domainname -b ${HOSTNAME}.${DOMAIN}
hostnamectl set-hostname ${HOSTNAME}
sysctl kernel.hostname
systemctl restart networking.service || /etc/init.d/networking restart

cat <<-'EOF' >/etc/apt/sources.list
# Dépôt de base de Debian Jessie
deb http://httpredir.debian.org/debian/ jessie main contrib
deb-src http://httpredir.debian.org/debian/ jessie main contrib

# Mises à jour distribution stable
deb http://httpredir.debian.org/debian/ jessie-updates main
deb-src http://httpredir.debian.org/debian/ jessie-updates main

# Mises à jour vers distribution stable
deb http://httpredir.debian.org/debian/ jessie-backports main
deb-src http://httpredir.debian.org/debian/ jessie-backports main

# Mises à jour de sécurité
deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main
EOF

apt-get update
apt-get -fy upgrade && apt-get -fy dist-upgrade
apt-get -fy install nginx spawn-fcgi php5-fpm php5-cli php5-json php5-curl

systemctl stop php5-fpm.service
systemctl stop nginx.service

cp /etc/nginx/nginx.conf{,.bak}
cat <<-'EOF' >/etc/nginx/nginx.conf
user                 www-data;
worker_processes     auto;
worker_rlimit_nofile 2000;

pid                  /var/run/nginx.pid;

events {
    worker_connections 1024;
    use                epoll;
    multi_accept       on;
}

http {
    open_file_cache          max=2000 inactive=20s;
    open_file_cache_valid    30s;
    open_file_cache_min_uses 2;
    open_file_cache_errors   on;

    log_format main   '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log /var/log/nginx/access.log main;
    error_log /var/log/nginx/error.log warn;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    reset_timedout_connection on;
    client_body_timeout       10;
    send_timeout              2;

    gzip              on;
    gzip_disable      "MSIE [1-6]\.(?!.*SV1)";
    gzip_comp_level   2;
    gzip_min_length   1000;
    gzip_proxied      expired no-cache no-store private auth;
    gzip_types        text/plain application/x-javascript text/xml text/css application/xml;

    server_tokens     off;
    server_names_hash_bucket_size 2048;

    ssl_protocols             TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
    ssl_prefer_server_ciphers on;

    include           /etc/nginx/mime.types;
    default_type      application/octet-stream;
    charset           UTF-8;
    include           /etc/nginx/conf.d/*.conf;
    include           /etc/nginx/sites-enabled/*;
}

EOF

cat <<-'EOF' >/etc/nginx/sites-available/appli1
upstream phpfcgi {
    # server php:9000;
    server unix:/var/run/php5-fpm.sock;
}

server {
    listen 80;
    listen [::]:80;
    server_name appli1.miletrie.lan;

    ##
    # Index
    #
    index index.php index.html index.htm;

    ##
    # Journalisation
    #
    access_log /var/log/nginx/appli1/access.log;
    error_log /var/log/nginx/appli1/error.log;

    ##
    # Racine
    #
    root /usr/share/nginx/webroot/appli1;

    ###
    # Location
    #
    location / {
        try_files $uri $uri/ =404;
    }

    location ~ \.php$ {
        try_files                     $uri =404;
        fastcgi_index                 index.php;
        fastcgi_intercept_errors      on;
        fastcgi_pass                  phpfcgi;
        fastcgi_split_path_info       ^(.+\.php)(/.+)$;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include                       /etc/nginx/fastcgi_params;
    }

    location ~* \.(ico|css|js|gif|jpe?g|png)$ {
        expires       max;
        log_not_found off;
        access_log    off;
        add_header    Pragma public;
        add_header    Cache-Control "public, must-revalidate, proxy-revalidate";
    }

    location ~ /\.ht {
        deny all;
    }

    error_page 404 /404.html;
#    error_page 500 502 503 504 /50x.html;

    location = /404.html {
        root /usr/share/nginx/static/;
    }

    location = /50x.html {
        root /usr/share/nginx/static/;
    }

}

EOF

rm /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/appli1 /etc/nginx/sites-enabled/appli1
install -m 755 -o root -g adm -d /var/log/nginx/appli1/
touch /var/log/nginx/appli1/{access,error}.log
chmod 640 /var/log/nginx/appli1/{access,error}.log
chown www-data:adm /var/log/nginx/appli1/{access,error}.log
install -m 755 -o root -g root -d /usr/share/nginx/{webroot,static}/
install -m 755 -o root -g root -d /usr/share/nginx/webroot/appli1/vendor/

cat <<-'EOF' >/usr/share/nginx/static/404.html
<!DOCTYPE html>
<html>
    <head>
        <title>404 Not Found</title>
        <style>
            body {
                width: 35em;
                margin: 0 auto;
                font-family: Tahoma, Verdana, Arial, sans-serif;
            }
        </style>
    </head>
    <body>
        <h1>404 Not Found</h1>
        <p>Sorry, the page you are looking for could not be found.<br/>
        The page may no longer exist or have moved to a new location.</p>
        <p><em>Faithfully yours, nginx.</em></p>
    </body>
</html>

EOF

cat <<-'EOF' >/usr/share/nginx/static/50x.html
<!DOCTYPE html>
<html>
    <head>
        <title>Error</title>
        <style>
            body {
                width: 35em;
                margin: 0 auto;
                font-family: Tahoma, Verdana, Arial, sans-serif;
            }
        </style>
    </head>
    <body>
        <h1>An error occurred.</h1>
        <p>Sorry, the page you are looking for is currently unavailable.<br/>
        Please try again later.</p>
        <p>If you are the system administrator of this resource then you should check
        the <a href="http://nginx.org/r/error_log">error log</a> for details.</p>
        <p><em>Faithfully yours, nginx.</em></p>
    </body>
</html>

EOF

git clone -b 1.3-stable https://github.com/Jasig/phpCAS.git /usr/share/nginx/webroot/appli1/vendor/phpCAS

systemctl restart php5-fpm.service
systemctl restart nginx.service
